source 'https://rubygems.org'

gem 'rails', '4.2.1'

# Supported DBs
gem 'sqlite3'

# Authentication and authorization libraries
gem 'devise'
gem 'bcrypt-ruby', '3.1.2'
gem 'cancancan'

# Console
gem 'rb-readline' 

## Assets and front end gems
# Compiling and serving web assets
gem 'sprockets', '3.2.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Twitter Bootstrap
gem 'bootstrap-sass', '~> 3.3.1'
# Font awesome
gem 'font-awesome-sass'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Static pages
gem 'high_voltage', '~> 2.2.1'

# File attachements
gem "paperclip", "~> 4.2"

# Env vars handling
gem "figaro" 

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'rspec-rails', '3.1.0' 
  gem 'libnotify' if /linux/ =~ RUBY_PLATFORM
  gem 'growl' if /darwin/ =~ RUBY_PLATFORM
  gem 'guard-rspec', '4.5.0'
  gem 'spork-rails', '4.0.0'
  gem 'guard-spork', '2.1.0'
  gem 'factory_girl_rails', '4.2.0'
  gem 'faker'
end

group :test do
  gem 'capybara', '2.4.4' 
  gem 'cucumber-rails', '1.3.0', :require => false
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
end

group :production do
  # Supported DBs (only production)
  gem 'mysql2', platform: :ruby
  gem 'jdbc-mysql', platform: :jruby
  gem 'activerecord-jdbc-adapter', platform: :jruby

  gem 'rails_12factor', '0.0.2'
end